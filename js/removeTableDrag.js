(function ($) {
  Drupal.behaviors.term_condition = {
    attach: function (context, settings) {

      for ( var item in Drupal.settings.tableDrag ){
        //each item is a table id atribute
        var table = document.getElementById(item);
        $(table).find('td.field-multiple-drag').remove();
        $(table).find('td.delta-order').remove();
        $(table).find('th:last-child').remove();
      }

      //remove tableDrag settings on document load and each ajax success
      delete Drupal.settings.tableDrag;
      $(document).ajaxSuccess(function() {
        delete Drupal.settings.tableDrag;
      });
    }
  };
}(jQuery));
